import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';

import usersRoute from './routes/users';

const app = express();

// Middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());

// Routes
app.use('/users', usersRoute);

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  /* We log the error internaly */
  // logger.error(err);

  /*
  * Remove Error's `stack` property. We don't want
  * users to see this at the production env
  */
  if (req.app.get('env') !== 'development') {
    // eslint-disable-next-line no-param-reassign
    delete err.stack;
  }

  /* Finaly respond to the request */
  res.status(err.statusCode || 500).json(err);
});

// Start the server
const port = process.env.PORT || 3030;
app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server listening on port: ${port}`);
});
