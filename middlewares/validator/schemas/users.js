const schemas = {};

schemas.signin = {
  username: {
    in: 'body',
    isLength: {
      errorMessage: 'Username should be at least 7 chars long',
      // Multiple options would be expressed as an array
      options: { min: 7 },
    },
  },
  password: {
    in: 'body',
    isLength: {
      errorMessage: 'Password should be at least 6 chars long',
      // Multiple options would be expressed as an array
      options: { min: 7 },
    },
  },
};

export default schemas;
