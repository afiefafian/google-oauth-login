/* eslint-disable no-unused-vars */
import { checkSchema, validationResult } from 'express-validator/check';

// Custom error format
const errorFormatter = ({
  location, msg, param, value, nestedErrors,
}) => ({ msg });

const validator = schema => [
  checkSchema(schema),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(errorFormatter);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.mapped() });
    }
    return next();
  },
];

export default validator;
