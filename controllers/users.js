const controllers = {
  signIn: async (req, res, next) => {
    try {
      return res.json(req.body);
    } catch (error) {
      return next(error);
    }
  },
  signUp: async (req, res, next) => {
    try {
      return res.json(req.body);
    } catch (error) {
      return next(error);
    }
  },
  secret: async (req, res, next) => {
    try {
      return res.json(req.body);
    } catch (error) {
      return next(error);
    }
  },
};

export default controllers;
