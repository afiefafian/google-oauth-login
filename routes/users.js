import express from 'express';

import validator from '../middlewares/validator';
import userSchemas from '../middlewares/validator/schemas/users';
import UsersController from '../controllers/users';

const router = express.Router();

router.route('/signup')
  .post(UsersController.signUp);

router.route('/signin')
  .post(validator(userSchemas.signin), UsersController.signIn);

router.route('/secret')
  .post(UsersController.secret);

export default router;
